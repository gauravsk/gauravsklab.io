---
layout: page
title: " "
---

#### Hi. Thanks for visiting my website.

-------------

<div class="pull-left">

I am an ecologist, and I spend my time thinking about the origins, structure,  maintenance, and consequences of diversity.  
<br><br>
I believe that Black Lives Matter and that we need to work hard to overcome the pernicious effects of systemic racism and colonialism in the US and across the world.
</div>

<div class="pull-right" align="center">
<img src="assets/GSK_profile.jpg" height='240'>
<!--<small><i>Gaurav at the Salton Sea<br>with the <a href="https://bruinbirdingclub.org/">Bruin Birding Club</a></i></small>-->
</div>

<br>

I am an Assistant Professor at the Louisiana State University [Department of Biological Sciences](https://www.lsu.edu/science/biosci/). I came to LSU after working as a  postdoctoral researcher at the  [University of Missouri](https://biology.missouri.edu/), and completed my PhD at the University of California, Los Angeles. 

My research is generally focused on the ecology and evolution of species interactions. I am also deeply committed to making science a more inclusive endeavor by reimagining how we work in our classrooms and research communities.  For more details on my interests and process, please take a look around this site, including my [research interests and values](research). You can also browse [my CV](assets/GSK_CV.pdf) for an overview of where I've been and what I've done. <!--, and my [positionality statement]() for an overview of how I move through the world.  -->

If you are interested in joining my lab as student or postdoc, I would love to hear from you! Please see the [join](join) page for more information, and email me at [gaurav.kandlikar@gmail.com](mailto:gkandlikar@lsu.edu) to discuss opportunities.[^contact]

[^contact]: Contacting a potential faculty mentor can feel intimidating, especially if you are seeking out opportunities for undergraduate research or as a potential graduate student. [This post from Científico Latino](https://www.cientificolatino.com/post/contacting_prospectivesupervisor) provides great pointers on writing an initial contact email. Above all, please remember that if you are interested in joining my group after learning about my work, I will be as excited to meet with you as you will be about me!  And if you don't get a response from me within a week, please send a follow-up email - I promise I won't ever ignore your message on purpose!


![](assets/white-mtns.jpg)
 <div align="center">
<small><i>
Sunrise at the White Mountains
</i></small>
</div>

<br>

<!--
### Overview

My research focuses on the interplay of environmental variation and biotic interactions in ecological communities. Within this theme, I am especially interested in the community consequences of interactions between plants and their associated microorganisms, which have emerged as a dominant driver of plant dynamics across ecosystems. For more information about the projects that keep me occupied, visit the [projects](projects/) page, take a look at my [CV](assets/GSK_CV.pdf), and [email me](mailto::gaurav.kandlikar@gmail.com) with any questions.

I am also trying to make an effort to build my sense of environmental and educational ethics. Through reflecting on my own experiences, attending workshops, talking to students, and reading scholars, I have learned a great deal about the barriers that Black, Indigenous, Latino, and other minority students face in science classrooms. I am committed to doing my part in helping minimize these barriers, but I don't claim to have definitive answers. I am always eager to have conversations about this topic, and would be happy to get any reading recommendations.

--> 


