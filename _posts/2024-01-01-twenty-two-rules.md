---
layout: post
title: Twenty two rules of storytelling
categories: [dad]
---

This morning, I was paging through some old notebooks in which dad had jotted down assorted notes. I came across a list which he had entitled "Twenty-two rules of storytelling". I found them to be pretty fun and compelling, and so am jotting them down here. (From a short google search while transcribing dad's notes, I learned that these 'rules' originated from a series of tweets by a Pixar Directory/Storyteller).


Not all are profound, but most are at least helpful. Some of them are incredibly applicable to science writing, too. I've marked the ones I found most endearing, resonant, or reminiscent of conversations with dad, with **.

<!--more-->


1. You admire a character for trying more than for their success. **  

2. You gotta keep in mind what's interesting to you as an audience, not what's fun for you as the writer. They can be very different. 

3. Trying for a theme is important, but you won't see what the story is till you're at the end of it. Now, rewrite. 

4. Once upon a time there was \_\_\_\_. Every day \_\_\_\_. One day \_\_\_\_. Because of that \_\_\_\_. Because of that \_\_\_\_. Until finally \_\_\_\_. 

5. What is your character good at? Comfortable with? Throw the polar opposite at them. Challenge them.  How do they deal? 

6. Simplify. Focus. Combine characters. Hop over detours. You'll feel you're loosing valuable stuff but it sets you free. **  

7. Come up with an ending before you figure out your middle. Seriously. Endings are hard. Get yours working up front. 

8. Finish your story, let it go even if it is not perfect. In an ideal world you have both, but move on. Do better next time. **  

9. When you are stuck, make a list of what would NOT happen next. Lots of times the material to get unstuck will show up. ** 

10. Pull apart the stories you like. What you like in them is a part of you; you have got to recognize it before you can use it. ** 

11. Putting it on paper lets you start fixing it. If it stays in your head, a perfect idea, you will never share it with anybody. ** 

12. Discount the first thing that comes to your mind. Then the second, third, and fourth.... Get the obvious out of the way. Surprise yourself. ** 

13. Give your characters opinions. Passive/malleable might seem likeable to you as you write, but it is poison to the audience. 

14. Why must you tell THIS story? What is the burning belief in your body your story feeds off? THAT is the heart of it. 

15. If you were your character, in this situation how would you feel? Honesty lends credibility to unbelievable situations. 

16. What are the stakes? Give us the reason to root for your character. What hapens if they do not succeed? Stack the odds against. ** 

17. No work is ever wasted. If something does not work, let go and move on - it'll come back around to be useful later. ** 

18. You have to know yourself the difference between doing your best and fussing. Story is testing, not refining. ** 

19. Coincidences to get characters into trouble are great. Coincidences to get out of trouble are cheating. 

20. Exercise: take the building blocks of a movie you dislike. What would you do to rearrange them into what you do like? ** 

21. You gotta identify with your characters/situation, can't just write 'cool'. What would make YOU act that way?

22. What is the essence of your story? The most economical telling of it? If you know that, you can build out from there. 


Here are the notes in dad's writing. 

<table>
<tr>
<td>
<img src="/assets/dad-pg1.jpg" alt="drawing" width="300"/>
</td>
<td>
  <img src="/assets/dad-pg2.jpg" alt="drawing" width="300" align="center"/>
</td>
</tr>
</table>