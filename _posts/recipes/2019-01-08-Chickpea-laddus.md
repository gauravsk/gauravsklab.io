---
title: Besan (Chickpea/Garbanzo flour) laddu
categories: [recipes-dessert]
layout: post
hidden: true
---

#### Ingredients
1. 1 pound (or a little less) garbanzo bean flour
2. 1 pound butter
3. 2-3 cups of powdered sugar
4. 1 teaspoon cardamom

#### Steps
1. Melt butter 
2. Add in flour, and keep stirring till the mixture becomes "easy" to stir. It should take 5-10 minutes, depending on the pan. You will also see butter bubbling up from around the edges of the pan. 
3. Roast for 1-2 more minutes once you get to the stage described in the previous step.
4. Turn off the heat, and let the pan cool for ~10-15 minutes. 
5. Mix in the cardamom
6. Mix in the powdered sugar. You will need at least 2 cups, and probably 3, but you can do this by taste (the mixture shouldn't taste "flour-y")
7. Roll into laddus. These amounts should yield 30-35 laddus.