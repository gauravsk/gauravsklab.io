---
title: Sabudana khichdi
categories: [recipes-otherMeals]
layout: post
hidden: true
---

*Note*: can substitute eccini pasta for sabundana throughout this recipe

#### Ingredients
1. Sabudana (tapioca balls), 1 cup per person.
2. potato
3. Curry leaves
4. Peanut chutney

#### Steps
1. Soak sabudana overnight (just enough water to cover the sabudana)
2. The next morning, peel and cut a potato into thin slices
3. Add peanut chutney, salt, a bit of sugar, and cilantro to the soaked tapioca and mix well
4. Make a phodni of cumin seeds (be liberal), and also add fresh green chilli and curry leaves if available
5. Toss in potato, and add a tiny bit of chili powder and 1 tsp salt. Fry till the potato is cooked.
6. Once potatoes are cooked through, toss in sabudana.
7. Stir at medium heat for a while, then, cover and lower the heat. The sabudana should steam and become translucent.
