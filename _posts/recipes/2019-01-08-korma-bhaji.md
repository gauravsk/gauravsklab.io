---
title: Kurma (mixed vegetable) bhaji
categories: [recipes-bhaji]
layout: post
hidden: true
---

#### Ingredients
1. 1/2 Cauliflower
2. 1-2 small potatoes
3. 1 medium onion
4. 1 cup peas
5. 2 tomatoes
6. *Optional* 1-2 carrots, cilantro

#### Steps
1. Finely chop onion, ginger, and garlic using the prestige veggie copper. Make them as small as possible.
2. Make a phodni with jeera, mustard, and (after taking off the heat), hing, turmeric, chili powder.
3. Sautee the onion-ginger-garlic combo for about two minutes.
4. During this time, use the prestige chopper to finely chop up the tomato.
5. After about two minutes of sauteeing the onion, toss in peas and the chopped tomato.
6. If desied, add in some water by washing the prestige chopper.
7. Add masala (garam masala, goda masala, pav bhaji masala, or even dhane-jeera poweder will do). Also add salt and a pinch of sugar.
8. Taste the water. It should be slightly over-salty.
9. Add all other vegetables.
10. Sautee for for 2 minutes, add cilantro if available.
11. Pressure cook for just one whistle.
