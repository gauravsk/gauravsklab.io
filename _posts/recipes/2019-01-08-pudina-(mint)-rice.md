---
title: Pudina (mint) rice
categories: [recipes-rice]
layout: post
hidden: true
---

*Notes*: Use big pressure cooker for this recipe. Makes enough for 4-5 individual meals. 

#### Ingredients
1. Fresh mint (1 bunch)
2. Basmati rice (2 cups flat)
3. Fresh green chillis (1-2)
4. 6-8 cloves grated garlic (can leave out if needed)
5. Cashews or peanuts
6. Carrots
7. Peas 
8. (Optional) 1 small potato, diced.  
9. (Optional) Lime juice
10. Garam masala (if you prefer, can add a small cinnamon stick, 2 cardamom pods, 4 cloves, 6-8 peppercorns, 1 bay leaf)

#### Steps

0. Wash the rice and keep soaking aside in 3 cups of water. 
1. Grind mint leaves, chilli, and garlic together with some (1/2 tsp-ish) salt.   
2. Make standard phodni (mustard seeds, cumin seeds, dried red chilli, turmeric)  
3. Add peas and carrots and potato (if using). Sautee for a minute with some salt (1/2 tsp-ish).
4. Add cashews or peanuts.  Sautee for another minute (note that cashews burn easily so keep an eye on them!)  
5. Add the ground paste from Step 1. Sautee for 2-3 minutes (till there is no raw garlic smell).  
6. Add lemon juice 
7. Add soaked rice with all of the water. Stir thoroughly; taste water and add salt if needed (the water should be a little more salty than you want your food to be). 
8. Pressure cook for 1 whistle.  
9. Serve with yogurt and ghee. 