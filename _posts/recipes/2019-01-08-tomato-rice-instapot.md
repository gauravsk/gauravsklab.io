---
title: Tomato rice in Instapot
categories: [recipes-otherMeals]
layout: post
hidden: true
---

### Ingredients 
1. Rice (2 cups -- this yields 4ish servings) 
2. Can of tomato (pureed - you can either buy pureed, or just blend a can of diced tomato) 
3. Onion (optional) (1/2 onion would be fine, more is probably OK.)
4. Celery (optional)  (1-2 sticks is great)
5. Carrots (optional)  (1-2 carrots is great)
6. Ginger (optional)
7. Cashews (optional)
8. Phodni ingredients  
9. 2 cups of water

### Steps  

1. Turn on Sautee program on "normal" setting.  
2. After it says "hot", add oil. Cover the bottom of the pot well.  
3. When oil looks shimmery, add half a teaspoon of mustard seeds. Let sit until they pop (but beware that they may not get hot enough to pop, in which case just move on to the next step when you feel impatient enough).  
4. Add 1 teaspoon of cumin seeds, and a broken up dried chili.  
5. Add about 4 squeezes of hing (asafoetida).  
6. IF USING ONIONS, add them now. Salt them a bit and sautee for a bit, till they start cooking. 
7. Then, add chopped carrots and celery.  
8. Add quarter teaspoon of turmeric, and a quarter teaspoon of the red chili powder.
9. Add a shallow teaspoon of salt. 
10. Cancel the Sautee program, and add 1/2 teaspoon garam masala, and 1 teaspoon coriander powder (optional).  
11. Add pureed can of tomato and the washed rice. 
12. Add teaspoon of sugar.  
13. Grate in about an inch of ginger (optional).  
14. Add two more cups of water
15. Taste the water for salt. You will probably need to add ~ 1/2-1 teaspoon more. (The water you taste should feel a bit over-salty.)  
16. Put lid on, and turn on the "Rice" program, on the Normal setting, for 12 minutes.  
17. Turn the steam valve thing to the sealing position. 
18. Once the 12 minutes are up (NOTE that this will take ~20 minutes total, as the 12 minute countdown begins after the pressure is built up), wait for 10 additional minutes. 
19. You can serve with some fresh basil or mint leaves, if you'd like. You can also serve with some yogurt. 

