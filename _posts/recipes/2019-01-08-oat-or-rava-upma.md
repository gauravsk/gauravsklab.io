---
title: Oat or rava upma
categories: [recipes-otherMeals]
layout: post
hidden: true
---

*Note*: For normal (rava) upma, just substitute rava for oats...

#### Ingredients
1. Oats (old fashioned only! Quick cook won't work.
2. Onion
3. Ginger-garlic paste
4. Cashews or peanuts
5. Peas and/or carrots
6. Hot water

#### Steps
1. Make phodni as usual.  Add 1-2 dried red peppers; or 1-2 green peppers (leave these in big pieces- don't chop too finely!).
2. Add onion (chopped into 1cm^2 pieces) and ginger-garlic paste, and sautee for 2 minutes.
3. Add peas and carrots, if desired.
4. Toss in some cashews or peanuts.
5. Add 1 cup of oat (sub. rava) and stir it around for a bit -- 2 minutes or so, just till it gets some color.
6. Add 2.5 cups of recently-boiled water.
