---
layout: post
title: Week 1 - The nature of nature
categories: [ecoevothought]
---

## Readings

- [Bridging indigenous and scientific knowledge](https://science.sciencemag.org/content/352/6291/1274.full), by Jayalaxshmi Mistry and Andrea Berardi. 

	> A major reason for the limited engagement with indigenous knowledge is the persistence of epistemological differences,  and  the associated  politics of representation, within the social and governance context. Local ecological knowledge is seen as subjective, arbitrary, and based on qualitative observations of phenomena and change. Scientific knowledge, by contrast, is viewed as objective and rigorous, with precise measuring and empirical testing of events and trends confirming credibility and legitimacy.

- [Whose Conservation?](https://science.sciencemag.org/content/345/6204/1558.abstract), by Georgina Mace.

  > However, in recent years the emphasis has moved from a potentially overly utilitarian perspective -- managing nature to maximize the overall value of the human condition -- to a more nuanced one that recognizes the two-way, dynamic relationships between people and nature. This "people and nature" thinking emphasizes the importance of cultural structures and institutions for developing sustainable and resilient interactions between human societies and the natural environment. It operates at a range of scales from global to local and has intellectual origins in resource economics, social science, and theoretical ecology. 
	
- [The Trouble with Wilderness: Or, Getting Back to the Wrong Nature](https://faculty.washington.edu/timbillo/Readings%20and%20documents/Wilderness/Cronon%20The%20trouble%20with%20Wilderness.pdf), by William Cronon.  

  > That is why, when I think of the times I myself have come closest to experiencing what I might call the sacred in nature, I often find myself remembering wild places much closer to home. I think, for instance, of a small pond near my house where water bubbles up from limestone springs to feed a series of pools that rarely freeze in winter and so play home to waterfowl that stay here for the protective warmth even on the coldest of winter days, gliding silently through streaming mists as the snow falls from gray February skies. I think of a November evening long ago when I found myself on a Wisconsin hilltop in rain and dense fog, only to have the setting sun break through the clouds to cast an otherworldly golden light on the misty farms and woodlands below, a scene so unexpected and joyous that I lingered past dusk so as not to miss any part of the gift that had come my way. [...] What I celebrate about such places is not just their wildness, though that certainly is among their most important qualities; what I celebrate even more is that they remind us of the wildness in our own backyards, of the nature that is all around us if only we have eyes to see it.


- [The Organism as the Subject and Object of Evolution](http://joelvelasco.net/teaching/systematics/Lewontin%2083%20-%20organism%20as%20subject%20and%20object%20.pdf), by Richard Lewontin
  > The organism is, in part, made by the interaction of the genes and the environment, but the organism makes its environment and so again participates in its own construction. Finally, the organism, as it develops, constructs an environment that is a condition of its survival and reproduction, setting the conditions of natural selection. So the organism influences its own evolution, by being both the object of natural selection and the creator of the conditions of that selection.

## Poems  

- [There Are Birds Here](https://www.poetryfoundation.org/poetrymagazine/poems/56764/there-are-birds-here), by Jamaal May 
- [From "understory"](https://www.poetryfoundation.org/poetrymagazine/poems/58639/from-understory), by Craig Santos Perez 
- [You can't be an NDN person in today's world](https://www.poetryinvoice.com/poems/you-cant-be-ndn-person-todays-world), by Tommy Pico

## Material suggested during our conversation 

- [The Protection of Traditional Knowledge in Peru: A Comparative Perspective Perspective](https://openscholarship.wustl.edu/cgi/viewcontent.cgi?article=1258&context=law_globalstudies), by Susanne Clark, Isabel Lapeña, and  Manuel Ruiz
- [Nature Poem](https://blogs.commons.georgetown.edu/engl-090-fall2018/files/2015/01/PicoNaturePoem.pdf), by Tommy Pico 
- [My food history wasn't lost, it was stolen](http://www.sporkful.com/my-food-history-wasnt-lost-it-was-stolen/), podcast with Tommy Pico 
- [In the Nature of Cities - Urban Political Ecology and The Politics of Urban Metabolism](https://www.researchgate.net/publication/275035207_In_the_Nature_of_Cities_-_Urban_Political_Ecology_and_The_Politics_of_Urban_Metabolism), by Maria Kaika and Erik Swyngedouw  

## Next week  

We decided at the end of this conversation that in our next meeting, we will discuss narratives around "invasive" species, especially focusing on indigenous perspectives. 