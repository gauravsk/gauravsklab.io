---
layout: post
title: Day 1 at the writing gym
categories: [writinggym]
---

Here I am again. It's spring time, with months of good weather ahead of me. I'm getting back into running after a hiatus triggered by overwork. And since I'm all about whole-body exercises now (lie), I am also back to working out at the writing gym. 

Working out at the writing gym is going to be a test of my patience. Even though my daily life involves a fair bit of writing -- writing for grants, papers, lecture materials, seminar slides -- I always struggle to put words on paper in this context of a daily writing practice. I tab over to my email, I check my phone, I want to listen to some music. Everything apart from... just writing. 

This is not so different from my efforts in running. I ran my first 1-mile run in September 2006. Maybe it was August, because the cross country team started practice early. But regardless of the month, what I know to be true is that it was miserable for me. I must have taken half an hour to complete that mile, and I was physically exhausted. But I kept at it. I stuck with the team for that year, almost always acting as the cabooze. And eventually I ran a 5-mile course over an hour. And then I gave up on running, but I came back to it! 10 years later! And I ran a half marathon in under 2 hours! But then I had to step away again. I busted my knee. But then I eased back into it, and ran a marathon! 

Of course this isn't ideal -- it would have been nicer if I could say simply that I started running in August 2006 and have done it since, and have gotten consistently better. But that's OK, life happens. I have grown in other ways during that period. And similarly so with the writing gym, I think. 

I never thought about writing as a craft -- as anything more than just work I need to do to communicate myself -- until 2018. But that year I realized that writing is a mode of being. Some people can just *be* by playing tennis, or a videogame, or watching a movie, or baking. Or running. In the same way, writing is something that I can do when I just want to *be*. It can help me clear my head, create something delightful, and if it helps me practice a skill that I want to develop anyway, then great. 

And this brings me to an aspect of my personality/daily habits right now that I am not very proud of. My attention span has severely deteriorated. It has been ages since I just sat and read a book. (And when I did manage to finish books in the past two years, it has more to do with the brilliance of the writer than my own ability to concentrate). It's been years since I just sat and worked out a math problem or learned a new coding framework. Not just refined my skills but really gone from being a complete novice to something of a master. I keep telling myself that this erosion of my attention span/focus is due to working a job where I need to  constantly check email, or due to the need to constantly keep up with the news so that I can uphold my social responsibilities. But neither of this is entirely true, is it? I do maintain autonomy over my life and my time, and in this way I am so incredibly privileged! So I want to make the most of this privilege. 

I feel that developing my writing muscles will help me address this shortcoming. Because writing does help me clear my cluttered brain. And a clear brain will help me take more action. I hope. 