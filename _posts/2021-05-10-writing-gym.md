---
layout: post
title: Working out at the writing gym
---

I am a slow writer. I often spend an hour drafting an email that takes the recipient no more than two minutes to read (and maybe a few extra minutes to respond). Or it takes me days to write the outline for a manuscript. This happens despite constant self-reminders that when it comes to writing, *rewriting* is the real name of the game, and that a good first draft is one that helps me write a second draft. I am usually pleased with the end result of my writing, but as my network and responsibilities grow, this slow writing seems to be getting increasingly unsustainable. 

<!--more-->


Part of the problem, I think, is that I only write things that Matter. (Or at least they *seem* to matter). It is as though I were a runner, but only ran races. No training runs, no core exercises. That needs to change. As [Amit Varma](https://twitter.com/amitvarma) puts it, I need to work out at the writing gym.

So that's what I am going to try doing now. I will write about anything that piques my interest, whether it be science, policy, education, running, gardening, or whatever else. I can't expect my brain to be able to articulate thoughts into words that matter if I only rev up the engine at races; I need to give it adequate training. 

