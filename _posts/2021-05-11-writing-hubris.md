---
layout: post
title: Writing as an act of hubris
---

Do we write to communicate, or to think? All seasoned writers that I have met would have readily and enthusiastically agree that writing and thinking are inextricably intertwined, but we seem to lose sight of this when we evaluate or even simply read others (or our own?) work. So how do we respect the Janus in writing? 

<!--more-->  

![](https://upload.wikimedia.org/wikipedia/commons/a/a9/Janus1.JPG)

I think part of the issue is that we all begin our journey with writing as readers. All we see is the end product, which has almost always been filtered through multiple rounds of revisions by the writers, editors, copywriters, and so on. We never see the writer at work (unless you grew up with one). And so we are only exposed to the one face of writing: the face which appears confident, sure of itself, and definite. The other face -- the one filled with confusion and uncertainty -- remains hidden. 

Let me turn from the abstract to the concrete. Rather than trying to capture how I think others feel about writing, let me think about myself. I wrote yesterday that I am an incredibly slow writer. Why is that? Well, when I pick up a proverbial pen, I can't help but feel that I am embarking on an act of hubris. Are I really a writer? Confident, definite, sure of myself? No, I am not. The holes in my understanding shine more brightly than do the little tidbits that I understand. Words seem to defy my thoughts, and anyway, are my thoughts really that meaningful to begin with? 

The answer isn't so simple as "yes *beta*, they are meaningful, believe in yourself". Rather, the answer seems to be something more along the lines of "I don't know whether or not they are meaningful. But the only way to find out is to write." I want to constantly remind myself: don't write for an audience, don't write with a definite end in sight. The reward isn't the product, it is the process. What people do with the product, that is not my concern. 

The challenge with this seems to be twofold. (At least). First, most of what we read doesn't embody this emphasis on process over product. People writing scientific papers, news articles, and editorials, seem to be writing from some sort of intellectual high horse. Regardless of how uncertain the authors' other face might be, the one they put forward says, "Look upon my works, ye mighty, and despair!" And so we feel that our own writing should also project this air of confidence. The second dimension of the challenge here is that the incentive structures around us push us towards this very philosophy. "Anything that is worth doing, is worth doing poorly!" we hear. Yes, that is true, but only insofar as the journey to doing something well inevitably leads us through doing it poorly. It does not mean we should stop there. 

Perhaps herein lies the value of literature. Reading Murakami isn't about learning new information, or even about the lessons I learn that I can apply to my own life. It is about going down that journey, exploring what someone else is thinking. It is about the process.