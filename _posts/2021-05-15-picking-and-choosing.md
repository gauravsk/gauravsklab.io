---
layout: post
title: Picking and choosing our battles?
---

It's a gray, drizzly Saturday morning. I woke up early and listened to an interview with [Samnath Subramanian](https://en.wikipedia.org/wiki/Samanth_Subramanian) about JBS Haldane as I boiled a pot of chai and finished some dishes from my lazy Friday evening. I marveled at Haldane, a multidimensional man of many virtues and not without his share of imperfections. Ace geneticist, pioneer of the use of mathematics in biology, originator of the [primordial soup hypothesis](https://en.wikibooks.org/wiki/Structural_Biochemistry/The_Oparin-Haldane_Hypothesis) for the origin of life, popular science and sci-fi writer, trench fighter against the nationalists in the Spanish Civil War, longtime defender of Stalin*, and much more. 

<!--more-->  

After my chai was ready, and the interview over, I sat down to read Subramanian's lates writings. I found an essay about the perverse structure of the Covid vaccine industry, where [public investments lead directly to private profits](https://qz.com/2006390/taxpayers-are-paying-twice-or-more-for-the-covid-19-vaccine/). That led me to a longform pieces about the [botched vaccine rollout in India](https://scroll.in/article/994932/interview-indias-vaccine-plan-a-was-the-pandemic-disappearing-its-new-plan-makes-no-sense), where the government has set up a series of arcane regulations dictating who gets access to vaccines, and at what cost. I then remembered that that I meant to read more about Israel's attacks against the Palestinians over the past week, because I have been woefully undereducated about this important civil rights crisis. My timing was impeccable: I found out about [Israel's bombing of Al-Jazeera's tower](https://www.aljazeera.com/news/2021/5/15/palestinian-death-toll-in-gaza-occupied-west-bank-mounts-live-news) one minute after the story was up on their website. 

This was all a lot to process before 8am on a Saturday morning. I hadn't even started thinking about any of my worldly concerns: the friends and mentees I'm worried about and fighting for in my daily life, trying to hold academia and other systems accountable as they claim to become more anti-racist (can we at least start by becoming less overtly racist?), and so on. And then the common refrain came back to me: how do I pick and choose my battles? More to the point, *is it possible* to pick and choose my battles? All my mentors to date say yes, that it is not only possible but in fact necessary to pick and choose one's battles. But what remains unclear to me is how one does that, and why we are all told to pick and choose. I am not saying that we need to have answers, or to be fighting all battles at once. But being sensitive to the outcomes of these  battles, caring about the people who are struggling on the front lines, seems not only inevitable, but also healthy. 


\* some of these are not quite like the others...
