---
layout: post
title: Jose Saramago's Small Memories
categories: [bookreport]
---

I just finished reading [Jose Saramago's "Small Memories"](https://en.wikipedia.org/wiki/Memories_of_My_Youth). I have not read any of Saramago’s work before, but reading this memoir left me wanting to read more. It is a delightful, innocent little book- reflections on an assortment of memories from Saramago’s childhood. I was also charmed by the writing style. Saramago seems to love writing long, complicated sentences, with many parenthetical asides. He writes like he thinks. I heard in [Steven Pinker's lecture on Style and Writing](https://www.youtube.com/watch?v=OV5J6BfToSw) (for I have not read the book myself) that Darwin wrote in *Descent of Man* that "man has an instinctive tendency to speak, as we see in the babble of our young children; whilst no child has an instinctive tendency to brew, bake, or write"; well, Saramago seems to defy this by instinctively merging writing and thinking (but maybe it was not instinctive for him; maybe he labored over all of his sentences to achieve his desired style). It worked for him:

> Some of the more significant writing of the past thirty years has taken delight in the long, lawless sentence—think of Thomas Bernhard, Bohumil Hrabal, W. G. Sebald, Roberto Bolaño—but no one sounds quite like Saramago. He has an ability to seem wise and ignorant at the same time, as if he were not really narrating the stories he narrates. Often, he uses what could be called unidentified free indirect style—his fictions sound as if they were being told not by an author but by, say, a group of wise and somewhat garrulous old men, sitting down by the harbor in Lisbon, having a smoke, one of whom is the writer himself.  
*From [James Wood's review of Death Takes a Holiday](https://www.newyorker.com/magazine/2008/10/27/death-takes-a-holiday)*

This book felt especially dear to me because in it I found two of my dad's boarding passes from 2010. Evidently he flew from Hyderabad to Kolkota and back in January 2010. I would have been in high school then. Who knew that nine years after this flight (that I was not even on), I would be holding onto it as one of my own small memories. 

I am somewhat disappointed in myself for having overwhelmingly read male authors recently (Murakami, Manu Pillai, EO Wilson, Sasha Issenberg, maybe others), and want to rectify this bias. I could start with Joan Didion, who dad frequently read, Toni Morrison, Louise Erdrich, Yaa Gyasi, or Chimamanda Ngozi Adichie. 