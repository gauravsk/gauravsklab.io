---
layout: post
title: Around the web (week of 10 February)
---

#### Reading

- [Searching for patterns, hunting for causes: Robert Macarthur, the mathematical naturalist](https://static1.squarespace.com/static/5224f8d1e4b02f25c1dde748/t/549613cee4b08da3f826571d/1419121614320/Searching+for+patterns%2C+hunting+for+causes+robert+macarthur%2C+the+mathematical+naturalist.pdf), by Jay Odenbaugh
> Ecological patterns, about which we construct theories, are only interesting if they are repeated. They may be repeated in space or time, and they may be repeated from species to species. A pattern which has all of these kinds of repetition is of special interest because of its generality, and yet these very general events are only seen by ecologists with rather blurred vision. The very sharp-sighted always find discrepancies and are able to say that there is no generality, only a spectrum of special cases.

- [A review of "Rediscovering our natural instinct"](https://undark.org/article/rediscovering-our-nature-instinct/) by M.R. O'Connor for Undark Magazine

- [The roots of writing lie in hopes and dreams, not in accounting](https://aeon.co/essays/the-roots-of-writing-lie-in-hopes-and-dreams-not-in-accounting), by  Michael Erard for Aeon.

- [You're probably using the wrong dictionary](http://jsomers.net/blog/dictionary), by James Somers

#### Listening  

- A brief lecture on [Stylish Academic Writing](https://www.youtube.com/watch?v=nQsRvAVSVeM) by Helen Sword  

- [Mathematical Ecology: A Century of Progress, and Challenges for the Next Century](https://www.youtube.com/watch?v=m6F6MRrDtrQ), by Simon Levin  

- [Flor de Toloache](http://mariachinyc.com/) is coming to the [Levitt Pavilion concert series](http://concerts.levittlosangeles.org/) in LA this August!