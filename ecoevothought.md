---
layout: page
title: Eco-Evo Thought Reading Group
---


{% for post in site.posts %}
{% if post.categories contains "ecoevothought"  %}
  * {{ post.date | date_to_string }} &raquo; [ {{ post.title }} ]({{ post.url }})
{% endif %}
{% endfor %}
