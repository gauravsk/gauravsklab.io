---
layout: page
title: Lab notebook
---

I am going to keep track of my work progress here. I will try to update this daily, or at the very least weekly.

<ol>
  {% for post in site.categories.labnotebook %}
    {% if post.url %}
        <li><a href="{{ post.url }}">{{ post.title }}</a></li>
    {% endif %}
  {% endfor %}
</ol>

