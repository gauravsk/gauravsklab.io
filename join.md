---
layout: page
title: " "
---

<!--
<p  class="tab" 
    style="background: lavenderblush; 
    padding: 10px; 
    border: 1px solid lightgray; 
    margin: 5px;">
<i>
<b>This page is under construction (September 2022).</b> <br>
If you feel you might be interested in joining my lab but want to talk more about what life in my lab and at LSU might look like, I'd love to chat! Please send me an <a href="mailto:gaurav.kandlikar@gmail.com">email</a>. I am excited to hear from you! 
</i>
</p> 
-->

#### Thanks for your interest in joining my group.

Over the coming years, I will be recruiting students, postdocs, and other lab members to grow my research group at Lousiana State University. I am eager to work with students and postdocs from a wide range of backgrounds, and I am committed to changing the culture of ecology to be more inclusive of students and scholars from background that are largely excluded from our field. Please [get in touch](mailto:gaurav.kandlikar@gmail.com) if you might be interested in working with me.[^contact]

----------

<u><i>Graduate students</i></u>: I am ready to welcome graduate students to start with me at LSU as early as Fall 2023. I am open to working with students on a wide range of ecological questions, field systems, and approaches. All graduate students in my lab can expect a baseline of funding (salary plus research funding) from a combination of departmental Teaching Assistantships and lab-supported Research Assistantships. I will also work closely with students to secure additional funding from external fellowships and grants.  

<details  class="tab" 
    style="background: lavenderblush; 
    padding: 10px; 
    border: 1px solid lightgray; 
    margin: 5px;">
<summary>
<i><b>Graduate students who wish to start in Fall 2023 need to submit their application to LSU by mid-December 2022. Please click here for more details.</b></i>  </summary>
<br>
Please <a href="mailto:gaurav.kandlikar@gmail.com">email me</a> by November 1 2022 if you think you might be interested in joining LSU as a grad student in Fall 2023. I'd love to meet with you and answer any questions about your potential fit in my lab and at LSU, the logistics of applying to graduate school, my approach to mentoring graduate students, and whatever other doubts or ideas you might have. I can also connect you with current and previous LSU students who can talk about other aspects of life at and around LSU. 

<br><br>

For more details about graduate applications about LSU, please check out the <a href="https://www.lsu.edu/science/biosci/programs/graduate/prospective-students.php">Prospective Students</a> page on the department website. International applicants should also read through the <a href="https://www.lsu.edu/graduateschool/admissions/internationaladmissions.php">International Admissions</a> page. I will be a member of the Systematics, Ecology, and Evolution division - you can also take a look at <a href="https://www.lsu.edu/science/biosci/divisions/see.php">other faculty in this division</a>, who might be future committee members or co-advisors.

</details>


-----------

<u><i>Postdocs</i></u>: I will advertise for a postdoc to join the lab in 2024. In addition to this internally funded position, I am always eager to collaborate with potential postdocs on independent fellowships  proposals, e.g. through NSF, LSRF, or other agencies. 

---------------

<u>Lab values</u>: My lab will have a deliberate dual focus on natural sciences and on critically evaluating and re-imagining how we do science. <!--In doing this work I will draw inspiration and lessons from scholars like [Dr. Max Liboiron](https://civiclaboratory.nl/), [Dr. Terrell Morton](https://scholar.google.com/citations?user=Yfwn2f1mrrYC&hl=en&oi=ao), and [Dr. Ambika Kamath](https://ambikamath.com/2020/02/23/doing-the-work/), whose work has helped me define my own aspirations.--> Over time, this website will include an articulation of the values that will guide my lab. In the mean time, I would be happy to discuss my vision for lab values in individual conversations. 


[^contact]: Contacting a potential faculty mentor can feel intimidating, especially if you are seeking out opportunities for undergraduate research or as a potential graduate student. [This post from Científico Latino](https://www.cientificolatino.com/post/contacting_prospectivesupervisor) provides great pointers on writing an initial contact email. Above all, please remember that if you are interested in joining my group after learning about my work, I will be as excited to meet with you as you will be about me!  And if you don't get a response from me within a week, please send a follow-up email - I promise won't ever ignore your message on purpose!


<br><br>

![](../assets/CR.jpg)

<div align="center">
<small><i>
A páramo in Costa Rica, which I had the privilege of visiting as a student in the OTS Tropical Plant Systematics course. Students in my lab will have the opportunity to enroll in this or other OTS courses. 
</i></small>
</div>
