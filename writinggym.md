---
layout: page
title: Here is my personal writing gym
---


{% for post in site.posts %}
{% if post.categories contains "writinggym"  %}
  * {{ post.date | date_to_string }} &raquo; [ {{ post.title }} ]({{ post.url }})
{% endif %}
{% endfor %}
