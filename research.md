---
layout: page
title:
---

**Research topics**: My research is generally focused on the ecology and evolution of species interactions. Most of my work to date has focused on how abiotic and biotic interactions shape plant species coexistence and biodiversity patterns over environmental gradients. I tackle these questions by integrating ecological theory with empirical data, and I have conducted field work in systems ranging from grasslands to forests. My current focal research themes are listed [below](#current), and my complete publication list is available on [my CV](../assets/GSK_CV.pdf). I am always eager to learn and collaborate with students and colleagues on ideas and systems that I have not worked on before. 

**Research process**: The insights from our research are only as robust as the processes that led to the results. Science as a whole, and ecology in particular, has had several notable and painful reminders that many dominant research practices in our field are fundamentally flawed. Such flawed practices recreate societal inequity and oppression, and generate unreliable results. To this end, a major emphasis in my lab will be to continually reflect on how we conduct research, to embrace ethical research practices at all steps, and to advocate for such practices beyond our lab. I am always open to conversations about how we conduct research, including interdisciplinary collaborations that turn a critical eye to the field of ecology. 

<!--As a researcher and professor, I consider myself to be accountable to my immediate collaborators (including trainees), colleagues in my department and scientific network, and society at large-->

<br><br><br>

-----------------------------

#### Current research topics {#current}

1. [Soil microbial controls over plant communities](#pmi)
2. [Using functional traits to study demography and species interactions](#traits)
3. [Pedagogy of quantatitive topics in ecology and evolution](#pedagogy)
4. *Your idea here?* I am always eager to collaborate and learn with students and colleagues on ideas and systems that I have not worked on before.



##### 1. Soil microbial controls over plant communities {#pmi}

Plants are entangled in a wide variety of interactions with soil microorganisms, including pathogens and mutualists that directly alter plant growth, decomposers whose effects on nutrient cycling indirectly alter plant dynamics, and endosymbionts that alter their plant hosts’ physiology. These networks of interactions can give rise to complex feedbacks with surprising implications for many processes in plant communities, including species invasions, ecological succession, and the maintenance of biodiversity. I combine ecological theory and empirical studies to evaluate how soil microorganisms shape the diversity and dynamics of plant communities. 


<details>
<summary class="tab">Key publications</summary>
<p class="tab">
<br> 
Xinyi Yan, Jonathan Levine, and Gaurav Kandlikar. A quantitative synthesis of microbial effects on plant diversity.  Proceedings of the National Academy of Sciences. <a>https://doi.org/10.1073/pnas.2122088119</a>
<br><br>

Gaurav Kandlikar, Xinyi Yan, Jonathan Levine, Nathan Kraft. Soil microbes generate stronger
fitness differences than stabilization among California annual plants. The American Naturalist. <a>https://doi.org/10.1086/711662</a>. 
<br><br>

Gaurav Kandlikar, Christoper Johnson, Xinyi Yan, Nathan Kraft, and Jonathan Levine. Winning and losing with microbes: how microbially mediated fitness differences influence plant diversity.   Ecology Letters.  <a>https://doi.org/10.1111/ele.13280</a>
<br><br>
</p>
</details>
<br>

![](../assets/greenhouse.jpg)
<div align="center">
<small><i>
Seedlings of annual plants for a plant-microbe interaction experiment 
</i></small>
</div>

##### 2. Linking functional traits to demography and species interactions  {#traits}

A central aim in ecology, made even more compelling by rapid global change, is to predict species demography over environmental gradients. Although there is long history of measuring functional traits that capture plant species' ecological strategies, we still lack a clear understanding of how these traits relate to species' abundance, demography, and interactions. A second theme of my research addresses how variation in functional traits - both within and across species - helps us understand species' demographic response to environmental variation. 


<details>
<summary class="tab">Key publications</summary>
<p class="tab">
<br>
Gaurav Kandlikar, Andy Kleinhesselink, and Nathan Kraft. Functional traits predict species responses to environmental variation in a California grassland annual plant community. Journal of Ecology. <a>https://doi.org/10.1111/1365-2745.13845</a>
<br><br>
Gaurav Kandlikar, Marcel Caritá Vaz, et al. Low functional and phylogenetic turnover of melastomes along a Costa Rican elevational gradient. Journal of Tropical Ecology. <a>https://doi.org/10.1017/S0266467418000172</a>
</p>
</details>

<br> 
![](../assets/sedgwick.jpg)
 <div align="center">
<small><i>
Spring at Sedgwick Reserve
</i></small>
</div>

<br>

##### 3. Pedagogy of mathematical models in ecology and evolution {#pedagogy}  

Mathematics has propelled our ability to organize and interpret the wide range of ecological phenomena we observe in nature, but the same mathematical ideas that have enabled so much progress in ecology can also serve as a barrier to new students. Part of the problem is that there are longstanding societal barriers that alienate whole groups of curious students from embracing quantitative methods. I am committed to helping eliminate this barrier, especially as mathematical and computational approaches become increasingly central to our field. As a first step, I am working with colleagues to develop a new platform for interactive applications to help teach and learn quantitative models. For more information about this project, visit the [project website](https://ecoevoapps.gitlab.io) or read our [preprint](https://www.biorxiv.org/content/10.1101/2021.06.18.449026v1). And, contact me if you'd like to get involved.

<details>
<summary class="tab">Key publications</summary>
<p class="tab">
<br>
Rosa McGuire, Kenji Hayashi, Xinyi Yan, Marcel Caritá Vaz, Damla Cinoğlu, Madeline Cowen, Alejandra Martínez-Blancas, Lauren Sullivan, Sheila Vazquez-Morales, Gaurav Kandlikar. EcoEvoApps: interactive applications for teaching quantitative models in ecology and evolution. Preprint at  <a>https://doi.org/10.1101/2021.06.18.449026 </a>
<br><br>
</p>
</details>

<br> 

![](../assets/mac-rosen.gif)
 <div align="center">
<small><i>
Simulating the "paradox of enrichment" in a predator-prey model
</i></small>
</div>

------------





<!--
-----------

#### Other research and teaching projects {#other}

<u>How do consumers shape plant species coexistence?</u>  
The dynamics of natural plant communities are governed not only by interactions that directly occur between plant species, or between plants and microbes, but also by the actions of consumers. As part of my work at Sedgwick Reserve, I have collaborated with [Dr. Will Petry](https://wpetry.github.io/) to ask how granivorous ants that harvest the seeds of annual plant species promote or erode diversity of the plant community. 

<u>Using environmental DNA for biodiversity research</u>

During my PhD at UCLA, I worked with the California environmental DNA program CaleDNA to develop an [bioinformatics pipeline](https://www.biorxiv.org/content/early/2018/12/07/488627) to help automate the process of assigning taxonomy to sequences in eDNA projects. I also led the development of a [`R` package/`shiny` app](https://f1000research.com/articles/7-1734/v1) to help visualize results from environmental DNA sequencing studies which has been used by various undergraduate microbiology and eDNA classes at UCLA.
-->

