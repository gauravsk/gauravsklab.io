---
layout: page
title: Archive
---


{% for post in site.posts %}
{% unless post.categories contains "recipes" or post.categories contains "ecoevothought"  %}
  * {{ post.date | date_to_string }} &raquo; [ {{ post.title }} ]({{ post.url }})
{% endunless %}
{% endfor %}
